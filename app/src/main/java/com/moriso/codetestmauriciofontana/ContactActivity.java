package com.moriso.codetestmauriciofontana;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moriso.codetestmauriciofontana.data.ContactDAO;
import com.moriso.codetestmauriciofontana.model.Address;
import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.model.Email;
import com.moriso.codetestmauriciofontana.model.Phone;
import com.moriso.codetestmauriciofontana.util.ApplicationUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static com.moriso.codetestmauriciofontana.util.ApplicationUtil.contactListNeedsReload;

/**
 * Created by moriso on 24/09/17.
 */

public class ContactActivity extends AppCompatActivity {

    private Contact contact;

    private FloatingActionButton fabCall;
    private FloatingActionButton fabMessage;
    private FloatingActionButton fabEmail;

    private TextView tvContactName;
    private TextView tvContactBirthday;
    private List<TextView> tvAddressList;
    private List<TextView> tvPhoneList;
    private List<TextView> tvEmailList;

    private LinearLayout llContactAddresses;
    private LinearLayout llContactPhonesDynamic;
    private LinearLayout llContactAddressesDynamic;
    private LinearLayout llContactEmailsDynamic;

    private RelativeLayout rlBottomSheetEditContact;
    private BottomSheetBehavior bsb;

    private FloatingActionButton fabEditContact;

    private EditText etFirstName;
    private EditText etLastName;
    private EditText etBirthday;
    private EditText etAddress;
    private EditText etPhone;
    private EditText etEmail;
    private List<TextInputLayout> tiAddressList;
    private List<TextInputLayout> tiPhoneList;
    private List<TextInputLayout> tiEmailList;
    private List<EditText> etAddressList;
    private List<EditText> etPhoneList;
    private List<EditText> etEmailList;
    private TextView tvAddAddress;
    private TextView tvAddPhone;
    private TextView tvAddEmail;
    private LinearLayout llAddress;
    private LinearLayout llPhone;
    private LinearLayout llEmail;
    private FloatingActionButton fabSave;

    private int etAddressNumber = 1;
    private int etPhoneNumber = 1;
    private int etEmailNumber = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contact);

        contact = ApplicationUtil.getSelectedContact(getApplicationContext());

        setupMainLayout();
        setupEditLayout();

    }

    private void setupMainLayout() {

        fabCall = (FloatingActionButton) findViewById(R.id.fabCall);
        fabMessage = (FloatingActionButton) findViewById(R.id.fabMessage);
        fabEmail = (FloatingActionButton) findViewById(R.id.fabEmail);

        tvContactName = (TextView) findViewById(R.id.tvContactName);
        tvContactBirthday = (TextView) findViewById(R.id.tvContactBirthday);

        llContactAddresses = (LinearLayout) findViewById(R.id.llContactAddresses);
        llContactPhonesDynamic = (LinearLayout) findViewById(R.id.llContactPhonesDynamic);
        llContactAddressesDynamic = (LinearLayout) findViewById(R.id.llContactAddressesDynamic);
        llContactEmailsDynamic = (LinearLayout) findViewById(R.id.llContactEmailsDynamic);

        tvContactName.setText(contact.getFirstName() + " " + contact.getLastName());
        tvContactBirthday.setText(contact.getBirthDay());

        tvAddressList = new ArrayList<TextView>();
        tvPhoneList = new ArrayList<TextView>();
        tvEmailList = new ArrayList<TextView>();

        if (contact.getAddressList() == null || contact.getAddressList().size() == 0) {
            llContactAddresses.setVisibility(View.GONE);
        } else {
            llContactAddresses.setVisibility(View.VISIBLE);
            for (int i = 0; i < contact.getAddressList().size(); i++) {
                addTextView(ApplicationUtil.ADDRESS, llContactAddressesDynamic, contact.getAddressList().get(i).getAddress(), i == 0 ? false : true);
            }
        }

        for (int i = 0; i < contact.getPhoneList().size(); i++) {
            addTextView(ApplicationUtil.PHONE, llContactPhonesDynamic, contact.getPhoneList().get(i).getPhone(), i == 0 ? false : true);
        }

        for (int i = 0; i < contact.getEmailList().size(); i++) {
            addTextView(ApplicationUtil.EMAIL, llContactEmailsDynamic, contact.getEmailList().get(i).getEmail(), i == 0 ? false : true);
        }

        fabCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contact.getPhoneList().size() > 1) {
                    chooseCallPhone();
                } else {
                    callPhone(contact.getPhoneList().get(0).getPhone());
                }
            }
        });

        fabMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contact.getPhoneList().size() > 1) {
                    choosePhoneMessage();
                } else {
                    messagePhone(contact.getPhoneList().get(0).getPhone());
                }
            }
        });

        fabEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contact.getEmailList().size() > 1) {
                    chooseEmail();
                } else {
                    sendEmail(contact.getEmailList().get(0).getEmail());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (bsb.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    private void setupEditLayout() {
        rlBottomSheetEditContact = (RelativeLayout) findViewById(R.id.rlEditContact);
        fabEditContact = (FloatingActionButton) findViewById(R.id.fabEditContact);

        bsb = BottomSheetBehavior.from(rlBottomSheetEditContact);
        bsb.setState(BottomSheetBehavior.STATE_HIDDEN);

        bsb.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (bsb.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    etFirstName.setError(null);
                    etLastName.setError(null);
                    etBirthday.setError(null);
                    etAddress.setError(null);
                    etPhone.setError(null);
                    etEmail.setError(null);

                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    fabEditContact.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        fabEditContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabEditContact.setVisibility(View.GONE);
                bsb.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        etAddressNumber = 1;
        etPhoneNumber = 1;
        etEmailNumber = 1;

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etBirthday = (EditText) findViewById(R.id.etBirthday);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etEmail = (EditText) findViewById(R.id.etEmail);

        tiAddressList = new ArrayList<TextInputLayout>();
        tiPhoneList = new ArrayList<TextInputLayout>();
        tiEmailList = new ArrayList<TextInputLayout>();

        etAddressList = new ArrayList<EditText>();
        etPhoneList = new ArrayList<EditText>();
        etEmailList = new ArrayList<EditText>();
        etAddressList.add(etAddress);
        etPhoneList.add(etPhone);
        etEmailList.add(etEmail);

        tvAddAddress = (TextView) findViewById(R.id.tvAddAddress);
        tvAddPhone = (TextView) findViewById(R.id.tvAddPhone);
        tvAddEmail = (TextView) findViewById(R.id.tvAddEmail);

        llAddress = (LinearLayout) findViewById(R.id.llAddress);
        llPhone = (LinearLayout) findViewById(R.id.llPhone);
        llEmail = (LinearLayout) findViewById(R.id.llEmail);

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etFirstName.getText() != null && etFirstName.getText().toString().length() > 0) {
                    if (!Character.isLetter(etFirstName.getText().charAt(0))) {
                        if (etFirstName.getText().toString().length() > 1)
                            etFirstName.setText(etFirstName.getText().toString().substring(1, etFirstName.getText().toString().length()));
                        else
                            etFirstName.setText("");
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etFirstName.setText(contact.getFirstName());
        etLastName.setText(contact.getLastName());
        etBirthday.setText(contact.getBirthDay());
        if (contact.getAddressList() != null && contact.getAddressList().size() > 0)
            etAddress.setText(contact.getAddressList().get(0).getAddress());
        etPhone.setText(contact.getPhoneList().get(0).getPhone());
        etEmail.setText(contact.getEmailList().get(0).getEmail());

        fabSave = (FloatingActionButton) findViewById(R.id.fabSave);

        if (contact.getAddressList().size() > 1 ){
            for (int i = 1; i < contact.getAddressList().size(); i++) {
                addInputView(ApplicationUtil.ADDRESS, contact.getAddressList().get(i).getAddress());
            }
        }

        if (contact.getPhoneList().size() > 1 ){
            for (int i = 1; i < contact.getPhoneList().size(); i++) {
                addInputView(ApplicationUtil.PHONE, contact.getPhoneList().get(i).getPhone());
            }
        }

        if (contact.getEmailList().size() > 1 ){
            for (int i = 1; i < contact.getEmailList().size(); i++) {
                addInputView(ApplicationUtil.EMAIL, contact.getEmailList().get(i).getEmail());
            }
        }

        fabSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateForm()) {
                    Snackbar.make(findViewById(R.id.clMain), "Please, fill all the obligatory fields.", Snackbar.LENGTH_LONG).show();
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.error);
                    mp.start();
                    return;
                }

                contact.setFirstName(etFirstName.getText().toString());
                contact.setLastName(etLastName.getText().toString());
                contact.setBirthDay(etBirthday.getText().toString());

                List<Address> addresses = contact.getAddressList();

                for (int i = 0; i < etAddressList.size(); i++ ){
                    if (i >= addresses.size()) {
                        addresses.add(new Address((long) -1, etAddressList.get(i).getText().toString()));
                    } else {
                        Address currentAddress = addresses.get(i);
                        currentAddress.setAddress(etAddressList.get(i).getText().toString());
                        addresses.set(i, currentAddress);
                    }
                }

                contact.setAddressList(addresses);

                List<Phone> phones = contact.getPhoneList();
                for (int i = 0; i < etPhoneList.size(); i++) {
                    if (i >= phones.size()) {
                        phones.add(new Phone((long) -1, etPhoneList.get(i).getText().toString()));
                    } else {
                        Phone currentPhone = phones.get(i);
                        currentPhone.setPhone(etPhoneList.get(i).getText().toString());
                        phones.set(i, currentPhone);
                    }
                }

                contact.setPhoneList(phones);

                List<Email> emails = contact.getEmailList();
                for (int i = 0; i < etEmailList.size(); i++) {
                    if (i >= emails.size()) {
                        emails.add(new Email((long) -1, etEmailList.get(i).getText().toString()));
                    } else {
                        Email currentEmail = emails.get(i);
                        currentEmail.setEmail(etEmailList.get(i).getText().toString());
                        emails.set(i, currentEmail);
                    }
                }

                contact.setEmailList(emails);

                ContactDAO contactDAO = new ContactDAO(getApplicationContext());
                contactDAO.open();

                contactDAO.putContact(contact);
                contact = contactDAO.getContact(contact.getId());

                contactDAO.close();

                bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);

                Snackbar.make(findViewById(R.id.clMain), "Contact successfully edited! " + ApplicationUtil.getEmojiByUnicode(0x1F389), Snackbar.LENGTH_LONG).show();
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.success);
                mp.start();

                clearMainContactLayout();
                clearEditContactLayout();
                setupMainLayout();
                setupEditLayout();

                contactListNeedsReload = true;
            }
        });

        tvAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInputView(ApplicationUtil.ADDRESS, "");
            }
        });

        tvAddPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInputView(ApplicationUtil.PHONE, "");
            }
        });

        tvAddEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInputView(ApplicationUtil.EMAIL, "");
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                DatePickerDialog dialog = new DatePickerDialog(ContactActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                        String monthStr = month < 10 ? "0" + (month + 1) : "" + (month + 1);

                        etBirthday.setText(monthStr + "/" + dayStr + "/" + year);
                    }
                },
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });
    }

    private void addTextView(int inputKind, LinearLayout parent, String text, boolean hasTopMargin) {
        TextView tvChild = new TextView(getApplicationContext());
        LinearLayout.LayoutParams lllp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lllp.setMargins(0, hasTopMargin == true ? ApplicationUtil.getDpAsPixel(getApplicationContext(), 8) : 0, 0 ,0);

        tvChild.setText(text);
        tvChild.setTextSize(16);
        tvChild.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.darkText));
        tvChild.setId(tvChild.generateViewId());

        switch (inputKind) {
            case ApplicationUtil.ADDRESS:
                tvAddressList.add(tvChild);
                break;
            case ApplicationUtil.PHONE:
                tvPhoneList.add(tvChild);
                break;
            case ApplicationUtil.EMAIL:
                tvEmailList.add(tvChild);
        }

        parent.addView(tvChild, lllp);
    }

    private void addInputView(int inputKind, String inputText) {

        LinearLayout.LayoutParams lllp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        EditText etNewInput = new EditText(getApplicationContext());
        etNewInput.setLayoutParams(lllp);
        etNewInput.setTextSize(16);
        etNewInput.setMaxLines(1);
        etNewInput.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.darkText));
        etNewInput.setBackgroundTintList(ColorStateList.valueOf(
                ContextCompat.getColor(getApplicationContext(), R.color.darkText)));
        etNewInput.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);

        lllp.setMargins(ApplicationUtil.getDpAsPixel(getApplicationContext(), 16),
                ApplicationUtil.getDpAsPixel(getApplicationContext(), 4),
                ApplicationUtil.getDpAsPixel(getApplicationContext(), 16),
                ApplicationUtil.getDpAsPixel(getApplicationContext(), 0));

        TextInputLayout tiNewInput = new TextInputLayout(ContactActivity.this);

        switch (inputKind) {
            case ApplicationUtil.ADDRESS:
                etAddressNumber++;
                etNewInput.setHint("Address " + etAddressNumber);
                etNewInput.setText(inputText);
                etNewInput.setInputType(InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
                tiNewInput.addView(etNewInput, lllp);
                tiNewInput.setId(tiNewInput.generateViewId());
                etAddressList.add(etNewInput);
                tiAddressList.add(tiNewInput);
                llAddress.addView(tiNewInput);
                break;
            case ApplicationUtil.PHONE:
                etPhoneNumber++;
                etNewInput.setHint("Phone " + etPhoneNumber);
                etNewInput.setText(inputText);
                etNewInput.setInputType(InputType.TYPE_CLASS_PHONE);
                tiNewInput.addView(etNewInput, lllp);
                tiNewInput.setId(tiNewInput.generateViewId());
                etPhoneList.add(etNewInput);
                tiPhoneList.add(tiNewInput);
                llPhone.addView(tiNewInput);
                break;
            case ApplicationUtil.EMAIL:
                etEmailNumber++;
                etNewInput.setHint("Email " + etEmailNumber);
                etNewInput.setText(inputText);
                etNewInput.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                tiNewInput.addView(etNewInput, lllp);
                tiNewInput.setId(tiNewInput.generateViewId());
                etEmailList.add(etNewInput);
                tiEmailList.add(tiNewInput);
                llEmail.addView(tiNewInput);
                break;
        }
    }

    private void clearMainContactLayout() {
        for (TextView tv :
                tvAddressList) {
            llContactAddressesDynamic.removeView(findViewById(tv.getId()));
        }

        for (TextView tv :
                tvPhoneList) {
            llContactPhonesDynamic.removeView(findViewById(tv.getId()));
        }

        for (TextView tv :
                tvEmailList) {
            llContactEmailsDynamic.removeView(findViewById(tv.getId()));
        }
    }

    private void clearEditContactLayout() {
        for (TextInputLayout ti :
                tiAddressList) {
            llAddress.removeView(findViewById(ti.getId()));
        }
        for (TextInputLayout ti :
                tiEmailList) {
            llEmail.removeView(findViewById(ti.getId()));
        }
        for (TextInputLayout ti :
                tiPhoneList) {
            llPhone.removeView(findViewById(ti.getId()));
        }

        tiAddressList = new ArrayList<TextInputLayout>();
        tiPhoneList = new ArrayList<TextInputLayout>();
        tiEmailList = new ArrayList<TextInputLayout>();
        etAddressList = new ArrayList<EditText>();
        etPhoneList = new ArrayList<EditText>();
        etEmailList = new ArrayList<EditText>();
        etAddressList.add(etAddress);
        etPhoneList.add(etPhone);
        etEmailList.add(etEmail);

        etFirstName.setText("");
        etLastName.setText("");
        etBirthday.setText("");
        etAddress.setText("");
        etPhone.setText("");
        etEmail.setText("");

        etAddressNumber = 1;
        etPhoneNumber = 1;
        etEmailNumber = 1;
    }

    private boolean validateForm() {
        boolean valid = true;

        String firstName = etFirstName.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            etFirstName.setError("Obligatory");
            valid = false;
        } else {
            etFirstName.setError(null);
        }

        String lastName = etLastName.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            etLastName.setError("Obligatory");
            valid = false;
        } else {
            etLastName.setError(null);
        }

        String birthday = etBirthday.getText().toString();
        if (TextUtils.isEmpty(birthday)) {
            etBirthday.setError("Obligatory");
            valid = false;
        } else {
            etBirthday.setError(null);
        }

        boolean wasPhoneInserted = false;
        for (EditText et :
                etPhoneList) {
            String phone = et.getText().toString();
            if (!TextUtils.isEmpty(phone)) {
                wasPhoneInserted = true;
                break;
            }
        }
        if (wasPhoneInserted) {
            etPhone.setError(null);
        } else {
            valid = false;
            etPhone.setError("Obligatory");
        }

        boolean wasEmailInserted = false;
        for (EditText et :
                etEmailList) {
            String email = et.getText().toString();
            if (!TextUtils.isEmpty(email)) {
                wasEmailInserted = true;
                break;
            }
        }
        if (wasEmailInserted) {
            etEmail.setError(null);
        } else {
            valid = false;
            etEmail.setError("Obligatory");
        }

        return valid;
    }

    public void chooseCallPhone() {
        List<String> phones = new ArrayList<String>();
        for (Phone phone :
                contact.getPhoneList()) {
            phones.add(phone.getPhone());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
        builder.setTitle("Select a phone to call");
        builder.setItems(phones.toArray(new CharSequence[contact.getPhoneList().size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callPhone(contact.getPhoneList().get(which).getPhone());
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        AlertDialog ad = builder.create();
        ad.getListView().setDivider(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.darkPurple)));
        ad.getListView().setDividerHeight(1);
        ad.show();
    }

    public void callPhone(String phone) {

        if (ActivityCompat.checkSelfPermission(ContactActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ContactActivity.this,
                    new String[]{Manifest.permission.CALL_PHONE}, ApplicationUtil.PERMISSION_CALL);
            return;
        }

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));

        startActivity(callIntent);
    }

    public void choosePhoneMessage() {
        List<String> phones = new ArrayList<String>();
        for (Phone phone :
                contact.getPhoneList()) {
            phones.add(phone.getPhone());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
        builder.setTitle("Select a phone to message");
        builder.setItems(phones.toArray(new CharSequence[contact.getPhoneList().size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                messagePhone(contact.getPhoneList().get(which).getPhone());
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        AlertDialog ad = builder.create();
        ad.getListView().setDivider(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.darkPurple)));
        ad.getListView().setDividerHeight(1);
        ad.show();
    }

    public void messagePhone(String phone) {
        Intent messageIntent = new Intent(Intent.ACTION_VIEW);
        messageIntent.setData(Uri.parse("sms:" + phone));
        startActivity(messageIntent);
    }

    public void chooseEmail() {
        List<String> emails = new ArrayList<String>();
        for (Email email :
                contact.getEmailList()) {
            emails.add(email.getEmail());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
        builder.setTitle("Select an email to message");
        builder.setItems(emails.toArray(new CharSequence[contact.getEmailList().size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendEmail(contact.getEmailList().get(which).getEmail());
            }
        });

        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        AlertDialog ad = builder.create();
        ad.getListView().setDivider(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.darkPurple)));
        ad.getListView().setDividerHeight(1);
        ad.show();
    }

    public void sendEmail(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});

        startActivity(Intent.createChooser(emailIntent, "Send mail via..."));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ApplicationUtil.PERMISSION_CALL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (contact.getPhoneList().size() > 1 ) {
                        chooseCallPhone();
                    } else {
                        callPhone(contact.getPhoneList().get(0).getPhone());
                    }
                }
                break;
        }
    }
}
