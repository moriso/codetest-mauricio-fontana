package com.moriso.codetestmauriciofontana;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moriso.codetestmauriciofontana.data.ContactDAO;
import com.moriso.codetestmauriciofontana.model.Address;
import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.model.Email;
import com.moriso.codetestmauriciofontana.model.Phone;
import com.moriso.codetestmauriciofontana.util.ApplicationUtil;
import com.moriso.codetestmauriciofontana.util.SwipeToRemoveUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;

import static com.moriso.codetestmauriciofontana.util.ApplicationUtil.contactListNeedsReload;

public class MainActivity extends AppCompatActivity {

    private SearchView svContacts;
    private RecyclerView rvMain;
    private RelativeLayout rlBottomSheetNewContact;
    private CoordinatorLayout clMain;
    private BottomSheetBehavior bsb;

    private RelativeLayout rlEmpty;

    private EditText etSearchContact;

    private FloatingActionButton fabAddContact;

    private EditText etFirstName;
    private EditText etLastName;
    private EditText etBirthday;
    private EditText etAddress;
    private EditText etPhone;
    private EditText etEmail;
    private List<TextInputLayout> tiAddressList;
    private List<TextInputLayout> tiPhoneList;
    private List<TextInputLayout> tiEmailList;
    private List<EditText> etAddressList;
    private List<EditText> etPhoneList;
    private List<EditText> etEmailList;
    private TextView tvAddAddress;
    private TextView tvAddPhone;
    private TextView tvAddEmail;
    private LinearLayout llAddress;
    private LinearLayout llPhone;
    private LinearLayout llEmail;
    private FloatingActionButton fabSave;

    private List<Contact> contactList;
    private MainAdapter ma;

    private int etAddressNumber = 1;
    private int etPhoneNumber = 1;
    private int etEmailNumber = 1;

    private OnListFragmentInteractionListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contactListNeedsReload = true;

        setContentView(R.layout.activity_main);

        rlEmpty = (RelativeLayout) findViewById(R.id.rlEmpty);

        clMain = (CoordinatorLayout) findViewById(R.id.clMain);
        rvMain = (RecyclerView) findViewById(R.id.rvMain);

        svContacts = (SearchView) findViewById(R.id.svContacts);

        fabAddContact = (FloatingActionButton) findViewById(R.id.fabAddContact);

        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etBirthday = (EditText) findViewById(R.id.etBirthday);
        etAddress = (EditText) findViewById(R.id.etAddress);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etEmail = (EditText) findViewById(R.id.etEmail);

        tiAddressList = new ArrayList<TextInputLayout>();
        tiPhoneList = new ArrayList<TextInputLayout>();
        tiEmailList = new ArrayList<TextInputLayout>();

        etAddressList = new ArrayList<EditText>();
        etPhoneList = new ArrayList<EditText>();
        etEmailList = new ArrayList<EditText>();
        etAddressList.add(etAddress);
        etPhoneList.add(etPhone);
        etEmailList.add(etEmail);

        tvAddAddress = (TextView) findViewById(R.id.tvAddAddress);
        tvAddPhone = (TextView) findViewById(R.id.tvAddPhone);
        tvAddEmail = (TextView) findViewById(R.id.tvAddEmail);

        llAddress = (LinearLayout) findViewById(R.id.llAddress);
        llPhone = (LinearLayout) findViewById(R.id.llPhone);
        llEmail = (LinearLayout) findViewById(R.id.llEmail);

        fabSave = (FloatingActionButton) findViewById(R.id.fabSave);

        etSearchContact = (EditText) svContacts.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            etSearchContact.setTextColor(getColor(R.color.eggshell));
            etSearchContact.setHintTextColor(getColor(R.color.eggshell));
        } else {
            etSearchContact.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.eggshell));
            etSearchContact.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.eggshell));
        }

        rlBottomSheetNewContact = (RelativeLayout) findViewById(R.id.rlNewContact);

        bsb = BottomSheetBehavior.from(rlBottomSheetNewContact);
        bsb.setState(BottomSheetBehavior.STATE_HIDDEN);

        bsb.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (bsb.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    fabAddContact.setVisibility(View.VISIBLE);

                    if (contactListNeedsReload) {
                        loadContactList();
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        fabAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bsb.setState(BottomSheetBehavior.STATE_EXPANDED);
                fabAddContact.setVisibility(View.GONE);
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                DatePickerDialog dialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String dayStr = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
                        String monthStr = month < 10 ? "0" + (month + 1) : "" + (month + 1);

                        etBirthday.setText(monthStr + "/" + dayStr + "/" + year);
                    }
                },
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
            }
        });

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etFirstName.getText() != null && etFirstName.getText().toString().length() > 0) {
                    if (!Character.isLetter(etFirstName.getText().charAt(0))) {
                        if (etFirstName.getText().toString().length() > 1)
                            etFirstName.setText(etFirstName.getText().toString().substring(1, etFirstName.getText().toString().length()));
                        else
                            etFirstName.setText("");
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        fabSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!validateForm()) {
                    Snackbar.make(findViewById(R.id.clMain), "Please, fill all the obligatory fields.", Snackbar.LENGTH_LONG).show();
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.error);
                    mp.start();
                    return;
                }

                String firstName = etFirstName.getText().toString();
                String lastName = etLastName.getText().toString();
                String birthday = etBirthday.getText().toString();

                List<Address> addresses = new ArrayList<Address>();
                for (EditText et:
                        etAddressList) {
                    if (et.getText().toString() != null && !et.getText().toString().equals(""))
                        addresses.add(new Address((long) 0, et.getText().toString()));
                }

                List<Phone> phones = new ArrayList<Phone>();
                for (EditText et:
                        etPhoneList) {
                    if (et.getText().toString() != null && !et.getText().toString().equals(""))
                        phones.add(new Phone((long) 0, et.getText().toString()));
                }

                List<Email> emails = new ArrayList<Email>();
                for (EditText et:
                        etEmailList) {
                    if (et.getText().toString() != null && !et.getText().toString().equals(""))
                        emails.add(new Email((long) 0, et.getText().toString()));
                }

                Contact contact = new Contact((long) 0, firstName, lastName, birthday,
                        phones, emails, addresses);

                ContactDAO contactDAO = new ContactDAO(getApplicationContext());
                contactDAO.open();

                contactDAO.postContact(contact);

                contactList = contactDAO.getAllContacts();
                ma.notifyDataSetChanged();
                bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);

                contactDAO.close();

                Snackbar.make(findViewById(R.id.clMain), "Contact successfully added! " + ApplicationUtil.getEmojiByUnicode(0x1F389), Snackbar.LENGTH_LONG).show();
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.success);
                mp.start();
                contactListNeedsReload = true;

                clearAddContactLayout();

            }
        });

        tvAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInputView(ApplicationUtil.ADDRESS);
            }
        });

        tvAddPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInputView(ApplicationUtil.PHONE);
            }
        });

        tvAddEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addInputView(ApplicationUtil.EMAIL);
            }
        });

    }

    @Override
    protected void onResume() {
        if (contactListNeedsReload)
            loadContactList();
        super.onResume();
    }

    private void addInputView(int inputKind) {

        LinearLayout.LayoutParams lllp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        EditText etNewInput = new EditText(getApplicationContext());
        etNewInput.setLayoutParams(lllp);
        etNewInput.setTextSize(16);
        etNewInput.setMaxLines(1);
        etNewInput.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.darkText));
        etNewInput.setHintTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorAccent));
        etNewInput.setBackgroundTintList(ColorStateList.valueOf(
                ContextCompat.getColor(getApplicationContext(), R.color.darkText)));
        etNewInput.setImeOptions(EditorInfo.IME_ACTION_DONE);

        lllp.setMargins(ApplicationUtil.getDpAsPixel(getApplicationContext(), 16),
                ApplicationUtil.getDpAsPixel(getApplicationContext(), 4),
                ApplicationUtil.getDpAsPixel(getApplicationContext(), 16),
                ApplicationUtil.getDpAsPixel(getApplicationContext(), 0));

        TextInputLayout tiNewInput = new TextInputLayout(MainActivity.this);

        switch (inputKind) {
            case ApplicationUtil.ADDRESS:
                etAddressNumber++;
                etNewInput.setHint("Address " + etAddressNumber);
                etNewInput.setInputType(InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
                tiNewInput.addView(etNewInput, lllp);
                tiNewInput.setId(tiNewInput.generateViewId());
                etAddressList.add(etNewInput);
                tiAddressList.add(tiNewInput);
                llAddress.addView(tiNewInput);
                break;
            case ApplicationUtil.PHONE:
                etPhoneNumber++;
                etNewInput.setHint("Phone " + etPhoneNumber);
                etNewInput.setInputType(InputType.TYPE_CLASS_PHONE);
                tiNewInput.addView(etNewInput, lllp);
                tiNewInput.setId(tiNewInput.generateViewId());
                etPhoneList.add(etNewInput);
                tiPhoneList.add(tiNewInput);
                llPhone.addView(tiNewInput);
                break;
            case ApplicationUtil.EMAIL:
                etEmailNumber++;
                etNewInput.setHint("Email " + etEmailNumber);
                etNewInput.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                tiNewInput.addView(etNewInput, lllp);
                tiNewInput.setId(tiNewInput.generateViewId());
                etEmailList.add(etNewInput);
                tiEmailList.add(tiNewInput);
                llEmail.addView(tiNewInput);
                break;
        }

    }

    private void clearAddContactLayout() {
        for (TextInputLayout ti :
                tiAddressList) {
            llAddress.removeView(findViewById(ti.getId()));
        }
        for (TextInputLayout ti :
                tiEmailList) {
            llEmail.removeView(findViewById(ti.getId()));
        }
        for (TextInputLayout ti :
                tiPhoneList) {
            llPhone.removeView(findViewById(ti.getId()));
        }

        tiAddressList = new ArrayList<TextInputLayout>();
        tiPhoneList = new ArrayList<TextInputLayout>();
        tiEmailList = new ArrayList<TextInputLayout>();
        etAddressList = new ArrayList<EditText>();
        etPhoneList = new ArrayList<EditText>();
        etEmailList = new ArrayList<EditText>();
        etAddressList.add(etAddress);
        etPhoneList.add(etPhone);
        etEmailList.add(etEmail);

        etFirstName.setText("");
        etLastName.setText("");
        etBirthday.setText("");
        etAddress.setText("");
        etPhone.setText("");
        etEmail.setText("");

        etAddressNumber = 1;
        etPhoneNumber = 1;
        etEmailNumber = 1;
    }

    private void loadContactList() {

        contactListNeedsReload = false;

        ContactDAO contactDAO = new ContactDAO(getApplicationContext());
        contactDAO.open();

        contactList = new ArrayList<Contact>();
        contactList = contactDAO.getAllContacts();

        contactDAO.close();

        if (contactList == null || contactList.size() == 0) {
            rlEmpty.setAlpha(0);
            rlEmpty.setVisibility(View.VISIBLE);
            rlEmpty.animate().alpha(1);
        } else {
            rlEmpty.setVisibility(View.GONE);
        }

        Collections.sort(contactList, new Comparator<Contact>() {
            public int compare(Contact c1, Contact c2) {
                return c1.getFirstName().toUpperCase().compareTo(c2.getFirstName().toUpperCase());
            }
        });

        char firstLetter = ' ';
        for (int i = 0; i < contactList.size(); i++) {
            if (contactList.get(i).getFirstName().toUpperCase().charAt(0) != firstLetter) {
                firstLetter = contactList.get(i).getFirstName().toUpperCase().charAt(0);
                Contact c = new Contact();
                c.setId((long) -1);
                c.setFirstName(String.valueOf(firstLetter).toUpperCase());
                contactList.add(i, c);
            }
        }

        mListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(Contact contact) {
                ApplicationUtil.setSelectedContact(getApplicationContext(), contact);
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        };

        ma = new MainAdapter(contactList, getApplicationContext(), mListener);
        RecyclerView.LayoutManager lMRvMain = new LinearLayoutManager(getApplicationContext());
        rvMain.setLayoutManager(lMRvMain);
        rvMain.setItemAnimator(new DefaultItemAnimator());

        rvMain.setAdapter(ma);

        ItemTouchHelper ith = new ItemTouchHelper(new SwipeToRemoveUtil(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP, MainActivity.this) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();
                final Contact contact = ((MainAdapter) rvMain.getAdapter()).getContact(position);

                String msg;

                if (contact.getId() == -1) {
                    msg = "Hey, you can't remove a letter separator!";

                    final AlertDialog ad = new AlertDialog.Builder(MainActivity.this)
                            .setTitle(msg)
                            .setNegativeButton("OKAY, SORRY :(", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    rvMain.getAdapter().notifyItemChanged(position);
                                }
                            })
                            .setCancelable(false)
                            .create();
                    ad.show();

                } else {

                    msg = "Are you sure you want to remove " +
                            contact.getFirstName() + " " + contact.getLastName() +
                            "?";
                    final AlertDialog ad = new AlertDialog.Builder(MainActivity.this)
                            .setTitle(msg)
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ContactDAO cDAO = new ContactDAO(getApplicationContext());
                                    cDAO.open();

                                    char firstLetter = contactList.get(position).getFirstName().toUpperCase().charAt(0);
                                    cDAO.deleteContact(contactList.get(position));
                                    contactList.remove(position);

                                    rvMain.getAdapter().notifyItemRemoved(position);
                                    Snackbar.make(findViewById(R.id.clMain), "Contact removed! ", Snackbar.LENGTH_LONG).show();
                                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.success);
                                    mp.start();
                                }
                            })
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    rvMain.getAdapter().notifyItemChanged(position);
                                }
                            })
                            .setCancelable(false)
                            .create();
                    ad.show();
                }
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return ((MainAdapter) rvMain.getAdapter()).isTextFilterEmpty();
            }

        });

        ith.attachToRecyclerView(rvMain);

        svContacts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ma.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ma.filter(newText);
                return true;
            }
        });

    }

    private boolean validateForm() {
        boolean valid = true;

        String firstName = etFirstName.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            etFirstName.setError("Obligatory");
            valid = false;
        } else {
            etFirstName.setError(null);
        }

        String lastName = etLastName.getText().toString();
        if (TextUtils.isEmpty(lastName)) {
            etLastName.setError("Obligatory");
            valid = false;
        } else {
            etLastName.setError(null);
        }

        String birthday = etBirthday.getText().toString();
        if (TextUtils.isEmpty(birthday)) {
            etBirthday.setError("Obligatory");
            valid = false;
        } else {
            etBirthday.setError(null);
        }

        boolean wasPhoneInserted = false;
        for (EditText et :
                etPhoneList) {
            String phone = et.getText().toString();
            if (!TextUtils.isEmpty(phone)) {
                wasPhoneInserted = true;
                break;
            }
        }
        if (wasPhoneInserted) {
            etPhone.setError(null);
        } else {
            valid = false;
            etPhone.setError("Obligatory");
        }

        boolean wasEmailInserted = false;
        for (EditText et :
                etEmailList) {
            String email = et.getText().toString();
            if (!TextUtils.isEmpty(email)) {
                wasEmailInserted = true;
                break;
            }
        }
        if (wasEmailInserted) {
            etEmail.setError(null);
        } else {
            valid = false;
            etEmail.setError("Obligatory");
        }

        return valid;
    }

    @Override
    public void onBackPressed() {
        if (bsb.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bsb.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Contact contact) ;

    }
}