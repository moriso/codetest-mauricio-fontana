package com.moriso.codetestmauriciofontana;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.moriso.codetestmauriciofontana.util.FontsOverride;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FontsOverride.setDefaultFont(this, "DEFAULT", "NotoSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "NotoSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "NotoSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "NotoSans-Regular.ttf");

        SharedPreferences settings = getSharedPreferences("prefs",0);
        boolean firstRun = settings.getBoolean("firstRun", false);
        if (!firstRun) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("firstRun", true);
            editor.commit();

            Intent intent = new Intent(BaseActivity.this, SplashActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(BaseActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
