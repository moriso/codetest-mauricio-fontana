package com.moriso.codetestmauriciofontana.model;

import java.util.List;

/**
 * Created by moriso on 22/09/17.
 */

public class Contact {

    private Long id;
    private String firstName;
    private String lastName;
    private String birthDay;
    private List<Phone> phoneList;
    private List<Email> emailList;
    private List<Address> addressList;

    public Contact() {
    }

    public Contact(Long id, String firstName, String lastName, String birthDay,
                   List<Phone> phoneList, List<Email> emailList, List<Address> addressList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDay = birthDay;
        this.phoneList = phoneList;
        this.emailList = emailList;
        this.addressList = addressList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public List<Phone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public List<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

}
