package com.moriso.codetestmauriciofontana.data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.moriso.codetestmauriciofontana.model.Address;
import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.model.Email;
import com.moriso.codetestmauriciofontana.model.Phone;
import com.moriso.codetestmauriciofontana.util.SQLiteHelper;

import java.util.ArrayList;

public class ContactDAO {

    private Context context;
    private SQLiteDatabase database;
    private SQLiteHelper sqLiteHelper;

    public ContactDAO(Context context) {
        this.context = context;
    }

    public void open() throws SQLException {
        sqLiteHelper = new SQLiteHelper(context);
        database = sqLiteHelper.getWritableDatabase();
    }

    public void close() {
        sqLiteHelper.close();
        database.close();
    }

    public ArrayList<Contact> getAllContacts() {

        ArrayList<Contact> contacts = new ArrayList<>();

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_CONTACT,
                null,
                null,
                null,
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Contact contact = new Contact(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        null,
                        null,
                        null
                );
                contacts.add(contact);
                cursor.moveToNext();
            }


            AddressDAO addressDAO = new AddressDAO(context);
            addressDAO.open();
            for (Contact contact :
                    contacts) {
                contact.setAddressList(addressDAO.getAllAddressesFromContact(contact));
            }
            addressDAO.close();

            PhoneDAO phoneDAO = new PhoneDAO(context);
            phoneDAO.open();
            for (Contact contact :
                    contacts) {
                contact.setPhoneList(phoneDAO.getAllPhonesFromContact(contact));
            }
            phoneDAO.close();

            EmailDAO emailDAO = new EmailDAO(context);
            emailDAO.open();
            for (Contact contact :
                    contacts) {
                contact.setEmailList(emailDAO.getAllEmailsFromContact(contact));
            }
            emailDAO.close();
        }

        cursor.close();

        return contacts;
    }

    public Contact getContact(Long id) {

        Contact contact = null;

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_CONTACT,
                null,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(id)
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            contact = new Contact(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    null,
                    null,
                    null
            );

            AddressDAO addressDAO = new AddressDAO(context);
            addressDAO.open();
            contact.setAddressList(addressDAO.getAllAddressesFromContact(contact));
            addressDAO.close();

            PhoneDAO phoneDAO = new PhoneDAO(context);
            phoneDAO.open();
            contact.setPhoneList(phoneDAO.getAllPhonesFromContact(contact));
            phoneDAO.close();

            EmailDAO emailDAO = new EmailDAO(context);
            emailDAO.open();
            contact.setEmailList(emailDAO.getAllEmailsFromContact(contact));
            emailDAO.close();

        }

        return contact;
    }

    public void postContact(Contact contact) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(SQLiteHelper.KEY_FIRST_NAME, contact.getFirstName());
        contentValues.put(SQLiteHelper.KEY_LAST_NAME, contact.getLastName());
        contentValues.put(SQLiteHelper.KEY_BIRTHDAY, contact.getBirthDay());

        long id = database.insert(SQLiteHelper.DATABASE_TABLE_CONTACT, null, contentValues);

        contact.setId(id);


        AddressDAO addressDAO = new AddressDAO(context);
        addressDAO.open();
        for (Address address :
                contact.getAddressList()) {
            addressDAO.postAddress(contact, address);
        }
        addressDAO.close();

        PhoneDAO phoneDAO = new PhoneDAO(context);
        phoneDAO.open();
        for (Phone phone :
                contact.getPhoneList()) {
            phoneDAO.postPhone(contact, phone);
        }
        phoneDAO.close();

        EmailDAO emailDAO = new EmailDAO(context);
        emailDAO.open();
        for (Email email:
                contact.getEmailList()) {
            emailDAO.postEmail(contact, email);
        }
        emailDAO.close();
    }

    public void deleteContact(Contact contact) {

        deleteDependencies(contact);

        database.delete(SQLiteHelper.DATABASE_TABLE_CONTACT,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(contact.getId())
                });
    }

    public void putContact(Contact contact) {
        Contact oldContact = getContact(contact.getId());
        ContentValues contentValues = new ContentValues();

        if (!oldContact.getFirstName().equals(contact.getFirstName())) {
            contentValues.put(SQLiteHelper.KEY_FIRST_NAME, contact.getFirstName());
        }
        if (!oldContact.getLastName().equals(contact.getLastName())) {
            contentValues.put(SQLiteHelper.KEY_LAST_NAME, contact.getLastName());
        }
        if (!oldContact.getBirthDay().equals(contact.getBirthDay())) {
            contentValues.put(SQLiteHelper.KEY_BIRTHDAY, contact.getBirthDay());
        }

        if (contentValues.size() > 0)
            database.update(SQLiteHelper.DATABASE_TABLE_CONTACT,
                    contentValues,
                    SQLiteHelper.KEY_ID + " = ?",
                    new String[] {
                            String.valueOf(contact.getId())
                    });

        AddressDAO addressDAO = new AddressDAO(context);
        addressDAO.open();
        for (Address address :
                contact.getAddressList()) {
            if (address.getId() == -1) {
                addressDAO.postAddress(contact, address);
            } else {
                addressDAO.putAddress(address);
            }
        }
        addressDAO.close();

        PhoneDAO phoneDAO = new PhoneDAO(context);
        phoneDAO.open();
        for (Phone phone :
                contact.getPhoneList()) {
            if (phone.getId() == -1) {
                phoneDAO.postPhone(contact, phone);
            } else {
                phoneDAO.putPhone(phone);
            }
        }
        phoneDAO.close();

        EmailDAO emailDAO = new EmailDAO(context);
        emailDAO.open();
        for (Email email :
                contact.getEmailList()) {
            if (email.getId() == -1) {
                emailDAO.postEmail(contact, email);
            } else {
                emailDAO.putEmail(email);
            }
        }
        emailDAO.close();

    }

    private void deleteDependencies(Contact contact) {
        if (contact.getAddressList() != null && contact.getAddressList().size() > 0) {
            AddressDAO addressDAO = new AddressDAO(context);
            addressDAO.open();

            for (Address address :
                    contact.getAddressList()) {
                addressDAO.deleteAddress(address);
            }

            addressDAO.close();
        }

        if (contact.getEmailList() != null && contact.getEmailList().size() > 0) {
            EmailDAO emailDAO = new EmailDAO(context);
            emailDAO.open();

            for (Email email :
                    contact.getEmailList()) {
                emailDAO.deleteEmail(email);
            }

            emailDAO.close();
        }

        if (contact.getPhoneList() != null && contact.getPhoneList().size() > 0) {
            PhoneDAO phoneDAO = new PhoneDAO(context);
            phoneDAO.open();

            for (Phone phone :
                    contact.getPhoneList()) {
                phoneDAO.deletePhone(phone);
            }

            phoneDAO.close();
        }
    }
}
