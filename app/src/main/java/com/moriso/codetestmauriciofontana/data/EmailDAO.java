package com.moriso.codetestmauriciofontana.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.moriso.codetestmauriciofontana.model.Email;
import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.util.SQLiteHelper;

import java.util.ArrayList;

/**
 * Created by moriso on 23/09/17.
 */

public class EmailDAO {

    private Context context;
    private SQLiteDatabase database;
    private SQLiteHelper sqLiteHelper;

    public EmailDAO(Context context) {
        this.context = context;
    }

    public void open() throws SQLException {
        sqLiteHelper = new SQLiteHelper(context);
        database = sqLiteHelper.getWritableDatabase();
    }

    public void close() {
        sqLiteHelper.close();
        database.close();
    }

    public ArrayList<Email> getAllEmailsFromContact(Contact contact) {

        ArrayList<Email> emails = new ArrayList<>();

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_EMAIL,
                null,
                SQLiteHelper.KEY_ID_CONTACT + " = ?",
                new String[] {
                        String.valueOf(contact.getId())
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Email email = new Email(
                        cursor.getLong(0),
                        cursor.getString(1)
                );
                emails.add(email);
                cursor.moveToNext();
            }
        }

        cursor.close();

        return emails;
    }

    public Email getEmail(Long id) {

        Email email = null;

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_EMAIL,
                null,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(id)
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            email = new Email(
                    cursor.getLong(0),
                    cursor.getString(1)
            );
        }

        return email;
    }

    public void postEmail(Contact contact, Email email) {
        if (email.getEmail() != null && !email.getEmail().equals("")) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(SQLiteHelper.KEY_EMAIL, email.getEmail());
            contentValues.put(SQLiteHelper.KEY_ID_CONTACT, contact.getId());

            database.insert(SQLiteHelper.DATABASE_TABLE_EMAIL, null, contentValues);
        }
    }

    public void putEmail(Email email) {
        Email oldEmail = getEmail(email.getId());

        if (email.getEmail() == null || email.getEmail().equals("")) {
            deleteEmail(email);
        } else {
            if (!oldEmail.getEmail().equals(email.getEmail())) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(SQLiteHelper.KEY_EMAIL, email.getEmail());

                database.update(SQLiteHelper.DATABASE_TABLE_EMAIL,
                        contentValues,
                        SQLiteHelper.KEY_ID + " = ?",
                        new String[] {
                                String.valueOf(email.getId())
                        });
            }
        }
    }

    public void deleteEmail(Email email) {
        database.delete(SQLiteHelper.DATABASE_TABLE_EMAIL,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(email.getId())
                });
    }
}
