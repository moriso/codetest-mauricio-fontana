package com.moriso.codetestmauriciofontana.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.moriso.codetestmauriciofontana.model.Address;
import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.util.SQLiteHelper;

import java.util.ArrayList;

/**
 * Created by moriso on 23/09/17.
 */

public class AddressDAO {

    private Context context;
    private SQLiteDatabase database;
    private SQLiteHelper sqLiteHelper;

    public AddressDAO(Context context) {
        this.context = context;
    }

    public void open() throws SQLException {
        sqLiteHelper = new SQLiteHelper(context);
        database = sqLiteHelper.getWritableDatabase();
    }

    public void close() {
        sqLiteHelper.close();
        database.close();
    }

    public ArrayList<Address> getAllAddressesFromContact(Contact contact) {

        ArrayList<Address> addresses = new ArrayList<>();

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_ADDRESS,
                null,
                SQLiteHelper.KEY_ID_CONTACT + " = ?",
                new String[] {
                        String.valueOf(contact.getId())
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Address address = new Address(
                        cursor.getLong(0),
                        cursor.getString(1)
                );
                addresses.add(address);
                cursor.moveToNext();
            }
        }

        cursor.close();

        return addresses;
    }

    public Address getAddress(Long id) {

        Address address = null;

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_ADDRESS,
                null,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(id)
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            address = new Address(
                    cursor.getLong(0),
                    cursor.getString(1)
            );
        }

        return address;
    }

    public void postAddress(Contact contact, Address address) {
        if (address.getAddress() != null && !address.getAddress().equals("")) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(SQLiteHelper.KEY_ADDRESS, address.getAddress());
            contentValues.put(SQLiteHelper.KEY_ID_CONTACT, contact.getId());

            database.insert(SQLiteHelper.DATABASE_TABLE_ADDRESS, null, contentValues);
        }
    }

    public void putAddress(Address address) {
        Address oldAddress = getAddress(address.getId());

        if (address.getAddress() == null || address.getAddress().equals("")) {
            deleteAddress(address);
        } else {
            if (!oldAddress.getAddress().equals(address.getAddress())) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(SQLiteHelper.KEY_ADDRESS, address.getAddress());

                database.update(SQLiteHelper.DATABASE_TABLE_ADDRESS,
                        contentValues,
                        SQLiteHelper.KEY_ID + " = ?",
                        new String[] {
                                String.valueOf(address.getId())
                        });
            }
        }
    }

    public void deleteAddress(Address address) {
        database.delete(SQLiteHelper.DATABASE_TABLE_ADDRESS,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(address.getId())
                });
    }
}
