package com.moriso.codetestmauriciofontana.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.moriso.codetestmauriciofontana.model.Phone;
import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.util.SQLiteHelper;

import java.util.ArrayList;

/**
 * Created by moriso on 23/09/17.
 */

public class PhoneDAO {

    private Context context;
    private SQLiteDatabase database;
    private SQLiteHelper sqLiteHelper;

    public PhoneDAO(Context context) {
        this.context = context;
    }

    public void open() throws SQLException {
        sqLiteHelper = new SQLiteHelper(context);
        database = sqLiteHelper.getWritableDatabase();
    }

    public void close() {
        sqLiteHelper.close();
        database.close();
    }

    public ArrayList<Phone> getAllPhonesFromContact(Contact contact) {

        ArrayList<Phone> phones = new ArrayList<>();

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_PHONE,
                null,
                SQLiteHelper.KEY_ID_CONTACT + " = ?",
                new String[] {
                        String.valueOf(contact.getId())
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Phone phone = new Phone(
                        cursor.getLong(0),
                        cursor.getString(1)
                );
                phones.add(phone);
                cursor.moveToNext();
            }
        }

        cursor.close();

        return phones;
    }

    public Phone getPhone(Long id) {

        Phone phone = null;

        Cursor cursor = database.query(SQLiteHelper.DATABASE_TABLE_PHONE,
                null,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(id)
                },
                null,
                null,
                SQLiteHelper.KEY_ID);

        if (cursor != null) {
            cursor.moveToFirst();
            phone = new Phone(
                    cursor.getLong(0),
                    cursor.getString(1)
            );
        }

        return phone;
    }

    public void postPhone(Contact contact, Phone phone) {
        if (phone.getPhone() != null && !phone.getPhone().equals("")) {
            ContentValues contentValues = new ContentValues();

            contentValues.put(SQLiteHelper.KEY_PHONE, phone.getPhone());
            contentValues.put(SQLiteHelper.KEY_ID_CONTACT, contact.getId());

            database.insert(SQLiteHelper.DATABASE_TABLE_PHONE, null, contentValues);
        }
    }

    public void putPhone(Phone phone) {
        Phone oldPhone = getPhone(phone.getId());

        if (phone.getPhone() == null || phone.getPhone().equals("")) {
            deletePhone(phone);
        } else {
            if (!oldPhone.getPhone().equals(phone.getPhone())) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(SQLiteHelper.KEY_PHONE, phone.getPhone());

                database.update(SQLiteHelper.DATABASE_TABLE_PHONE,
                        contentValues,
                        SQLiteHelper.KEY_ID + " = ?",
                        new String[] {
                                String.valueOf(phone.getId())
                        });
            }
        }

    }

    public void deletePhone(Phone phone) {
        database.delete(SQLiteHelper.DATABASE_TABLE_PHONE,
                SQLiteHelper.KEY_ID + " = ?",
                new String[] {
                        String.valueOf(phone.getId())
                });
    }
}
