package com.moriso.codetestmauriciofontana.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.google.gson.Gson;
import com.moriso.codetestmauriciofontana.model.Contact;

/**
 * Created by moriso on 24/09/17.
 */

public class ApplicationUtil {

    public static final int ADDRESS = 1;
    public static final int PHONE = 2;
    public static final int EMAIL = 3;

    public static final int PERMISSION_CALL = 100;

    public static boolean contactListNeedsReload;

    public static String getEmojiByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }

    public static int getDpAsPixel(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp*scale + 0.5f);
    }

    public static Contact getSelectedContact(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("codetestmauriciofontana", context.MODE_PRIVATE);
        return new Gson().fromJson(preferences.getString("selected_contact", null), Contact.class);
    }

    public static void setSelectedContact(Context context, Contact contact) {
        SharedPreferences preferences = context.getSharedPreferences("codetestmauriciofontana", context.MODE_PRIVATE);
        preferences.edit().putString("selected_contact", new Gson().toJson(contact, Contact.class)).commit();
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}
