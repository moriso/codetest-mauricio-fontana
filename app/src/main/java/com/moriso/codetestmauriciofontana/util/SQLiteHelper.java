package com.moriso.codetestmauriciofontana.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by moriso on 11/27/15.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME                = "contacts.db";
    public static final int DATABASE_VERSION                = 1;
    public static final String DATABASE_TABLE_CONTACT       = "contact";
    public static final String DATABASE_TABLE_PHONE         = "phone";
    public static final String DATABASE_TABLE_EMAIL         = "email";
    public static final String DATABASE_TABLE_ADDRESS       = "address";
    public static final String KEY_ID                       = "id";
    public static final String KEY_FIRST_NAME               = "first_name";
    public static final String KEY_LAST_NAME                = "last_name";
    public static final String KEY_BIRTHDAY                 = "birthday";
    public static final String KEY_ADDRESS                  = "address";
    public static final String KEY_EMAIL                    = "email";
    public static final String KEY_PHONE                    = "phone";
    public static final String KEY_ID_CONTACT               = "id_contact";

    public static final String CREATE_TABLE_CONTACT =
            "CREATE TABLE " + DATABASE_TABLE_CONTACT +
                    " (" +
                    KEY_ID +          " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_FIRST_NAME +  " TEXT NOT NULL, " +
                    KEY_LAST_NAME +   " TEXT, " +
                    KEY_BIRTHDAY +    " TEXT" +
                    ");";

    public static final String CREATE_TABLE_ADDRESS =
            "CREATE TABLE "+ DATABASE_TABLE_ADDRESS +
                    " ("+
                    KEY_ID +          " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_ADDRESS +     " TEXT NOT NULL, " +
                    KEY_ID_CONTACT +  " INTEGER REFERENCES " +
                    DATABASE_TABLE_CONTACT +
                    ");";

    public static final String CREATE_TABLE_EMAIL =
            "CREATE TABLE " + DATABASE_TABLE_EMAIL +
                    " (" +
                    KEY_ID +          " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_EMAIL +       " TEXT NOT NULL, " +
                    KEY_ID_CONTACT +  " INTEGER REFERENCES " +
                    DATABASE_TABLE_CONTACT +
                    ");";

    public static final String CREATE_TABLE_PHONE =
            "CREATE TABLE " + DATABASE_TABLE_PHONE +
                    " (" +
                    KEY_ID +          " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_PHONE +       " TEXT NOT NULL, " +
                    KEY_ID_CONTACT +  " INTEGER REFERENCES " +
                    DATABASE_TABLE_CONTACT +
                    ");";

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CONTACT);
        db.execSQL(CREATE_TABLE_ADDRESS);
        db.execSQL(CREATE_TABLE_EMAIL);
        db.execSQL(CREATE_TABLE_PHONE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
