package com.moriso.codetestmauriciofontana.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.moriso.codetestmauriciofontana.R;
import com.moriso.codetestmauriciofontana.util.ApplicationUtil;

/**
 * Created by moriso on 18/09/17.
 */

public abstract class SwipeToRemoveUtil extends ItemTouchHelper.SimpleCallback {

    Context mContext;

    public SwipeToRemoveUtil(int dragDirs, int swipeDirs, Context context) {
        super(dragDirs, swipeDirs);
        mContext = context;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView,
                                RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    public static final float ALPHA_FULL = 1.0f;

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            // Get RecyclerView item from the ViewHolder
            View itemView = viewHolder.itemView;

            Paint p = new Paint();
            Drawable d = ContextCompat.getDrawable(mContext, R.drawable.ic_delete_white_24dp);

            Bitmap icon = ApplicationUtil.drawableToBitmap(d);

            if (dX > 0) {

                p.setARGB(255, 255, 0, 0);

                c.drawRect((float) itemView.getLeft(), (float) itemView.getTop(), dX,
                        (float) itemView.getBottom(), p);

                c.drawBitmap(icon,
                        (float) itemView.getLeft() + ApplicationUtil.getDpAsPixel(mContext, 16),
                        (float) itemView.getTop() + ((float) itemView.getBottom() - (float) itemView.getTop() - icon.getHeight())/2,
                        p);
            } else {

                p.setColor(ContextCompat.getColor(mContext, R.color.cornellRed));

                c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                        (float) itemView.getRight(), (float) itemView.getBottom(), p);

                c.drawBitmap(icon,
                        (float) itemView.getRight() - ApplicationUtil.getDpAsPixel(mContext, 16) - icon.getWidth(),
                        (float) itemView.getTop() + ((float) itemView.getBottom() - (float) itemView.getTop() - icon.getHeight())/2,
                        p);
            }

            final float alpha = ALPHA_FULL - Math.abs(dX) / (float) viewHolder.itemView.getWidth();
            viewHolder.itemView.setAlpha(alpha);
            viewHolder.itemView.setTranslationX(dX);

        } else {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    }
}
