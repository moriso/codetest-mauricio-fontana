package com.moriso.codetestmauriciofontana;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moriso.codetestmauriciofontana.model.Contact;
import com.moriso.codetestmauriciofontana.util.ApplicationUtil;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ItemViewHolder> {

    private List<Contact> contactList;
    private List<Contact> contactListCopy;
    private Context context;
    private String textFilter;
    private final MainActivity.OnListFragmentInteractionListener mListener;

    public MainAdapter(List<Contact> contactList, Context context, MainActivity.OnListFragmentInteractionListener listener) {
        this.contactList = contactList;
        this.context = context;
        this.mListener = listener;
        contactListCopy = new ArrayList<Contact>();
        contactListCopy.addAll(contactList);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int i) {

        final Contact contact = contactList.get(i);

        itemViewHolder.mContact = contact;

        if (contact.getId() != -1) {
            itemViewHolder.tvContactName.setVisibility(View.VISIBLE);
            itemViewHolder.tvLetter.setVisibility(View.GONE);
            itemViewHolder.vLetterSeparator.setVisibility(View.GONE);

            if (i == getItemCount() - 1)
                itemViewHolder.vContactSeparator.setVisibility(View.GONE);
            else
                itemViewHolder.vContactSeparator.setVisibility(View.VISIBLE);

            itemViewHolder.tvContactName.setText(contact.getFirstName() + " " + contact.getLastName());

            itemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onListFragmentInteraction(itemViewHolder.mContact);
                }
            });
        } else {
            itemViewHolder.tvContactName.setVisibility(View.GONE);
            itemViewHolder.vContactSeparator.setVisibility(View.GONE);
            itemViewHolder.tvLetter.setVisibility(View.VISIBLE);
            itemViewHolder.vLetterSeparator.setVisibility(View.VISIBLE);

            itemViewHolder.tvLetter.setText(contact.getFirstName());

            itemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.content_contact, viewGroup, false);

        return new ItemViewHolder(itemView);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        protected View mView;
        protected TextView tvContactName;
        protected View vContactSeparator;
        protected TextView tvLetter;
        protected View vLetterSeparator;
        protected Contact mContact;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            vContactSeparator = itemView.findViewById(R.id.vContactSeparator);
            tvLetter = (TextView) itemView.findViewById(R.id.tvLetter);
            vLetterSeparator = itemView.findViewById(R.id.vLetterSeparator);
        }
    }

    public Contact getContact(int position) {
        return contactList.get(position);
    }

    public void filter(String text) {
        textFilter = text;
        contactList.clear();
        if(text.isEmpty()){
            contactList.addAll(contactListCopy);
        } else{
            text = text.toLowerCase();
            for(Contact contact: contactListCopy){
                if(contact.getFirstName().toLowerCase().contains(text) ||
                        (contact.getLastName() != null && contact.getLastName().toLowerCase().contains(text))){
                    contactList.add(contact);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void removeFromContactListCopy(int position) {
        contactListCopy.remove(position);
    }

    public boolean isTextFilterEmpty() {
        if (textFilter == null || textFilter.equals("") || textFilter.isEmpty())
            return true;
        return false;
    }
}
