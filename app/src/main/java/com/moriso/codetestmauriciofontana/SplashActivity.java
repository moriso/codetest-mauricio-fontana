package com.moriso.codetestmauriciofontana;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

/**
 * Created by moriso on 26/09/17.
 */

public class SplashActivity extends AppCompatActivity {

    private Button btProceed;
    private RelativeLayout rlSplashMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        btProceed = (Button) findViewById(R.id.btProceed);
        rlSplashMain = (RelativeLayout) findViewById(R.id.rlSplashMain);

        btProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rlSplashMain.setAlpha(0);
        rlSplashMain.setVisibility(View.VISIBLE);

        rlSplashMain.animate().alpha(1);
    }
}
